package minimarketdemo.model.core.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.*;



/**
 * The persistent class for the cliente database table.
 * 
 */
@Entity
@Table(name="cliente")
@NamedQuery(name="Cliente.findAll", query="SELECT c FROM Cliente c")
public class Cliente implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="cedula_cliente", unique=true, nullable=false, length=10)
	private String cedulaCliente;

	@Column(nullable=false, length=100)
	private String apellidos;
	
	@Column(length=100)
	private String direccion;

	@Column(nullable=false, length=100)
	private String nombres;

	//bi-directional many-to-one association to FacturaCab
	@OneToMany(mappedBy="cedulaCliente")
	private List<FacturaCab> facturaCabs;

	//bi-directional many-to-one association to PedidoCab
	@OneToMany(mappedBy="cedulaCliente")
	private List<PedidoCab> pedidoCabs;
	
	public Cliente() {
	}

	public String getCedulaCliente() {
		return this.cedulaCliente;
	}

	public void setCedulaCliente(String cedulaCliente) {
		this.cedulaCliente = cedulaCliente;
	}

	public String getApellidos() {
		return this.apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getDireccion() {
		return this.direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getNombres() {
		return this.nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public List<FacturaCab> getFacturaCabs() {
		return this.facturaCabs;
	}

	public void setFacturaCabs(List<FacturaCab> facturaCabs) {
		this.facturaCabs = facturaCabs;
	}

	public FacturaCab addFacturaCab(FacturaCab facturaCab) {
		getFacturaCabs().add(facturaCab);
		facturaCab.setCedulaCliente(this);

		return facturaCab;
	}

	public FacturaCab removeFacturaCab(FacturaCab facturaCab) {
		getFacturaCabs().remove(facturaCab);
		facturaCab.setCedulaCliente(null);

		return facturaCab;
	}

	public List<PedidoCab> getPedidoCabs() {
		return this.pedidoCabs;
	}

	public void setPedidoCabs(List<PedidoCab> pedidoCabs) {
		this.pedidoCabs = pedidoCabs;
	}

	public PedidoCab addPedidoCab(PedidoCab pedidoCab) {
		getPedidoCabs().add(pedidoCab);
		pedidoCab.setCedulaCliente(this);

		return pedidoCab;
	}

	public PedidoCab removePedidoCab(PedidoCab pedidoCab) {
		getPedidoCabs().remove(pedidoCab);
		pedidoCab.setCedulaCliente(null);

		return pedidoCab;
	}
}
