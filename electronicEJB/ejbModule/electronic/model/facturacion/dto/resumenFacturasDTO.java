package electronic.model.facturacion.dto;

import java.math.BigDecimal;

public class resumenFacturasDTO {

	private int anio;
	private int mes;
	private BigDecimal Total;

	public resumenFacturasDTO() {

	}

	public resumenFacturasDTO(int anio, int mes, BigDecimal total) {
		super();
		this.anio = anio;
		this.mes = mes;
		Total = total;
	}

	public int getAnio() {
		return anio;
	}

	public void setAnio(int anio) {
		this.anio = anio;
	}

	public int getMes() {
		return mes;
	}

	public void setMes(int mes) {
		this.mes = mes;
	}

	public BigDecimal getTotal() {
		return Total;
	}

	public void setTotal(BigDecimal total) {
		Total = total;
	}

}
