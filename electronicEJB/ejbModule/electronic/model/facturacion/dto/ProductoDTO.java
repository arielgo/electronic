package electronic.model.facturacion.dto;

import java.math.BigDecimal;

public class ProductoDTO {
	private Integer codigoProducto;
	private String descripcion;
	private Integer existencia;
	private String nombre;
	private BigDecimal precioUnitario;
	private String tieneImpuesto;
	private int cantCarrito;
	public ProductoDTO() {
		
	}
	public ProductoDTO(Integer codigoProducto, String descripcion, Integer existencia, String nombre,
			BigDecimal precioUnitario, String tieneImpuesto, int cantCarrito) {
		super();
		this.codigoProducto = codigoProducto;
		this.descripcion = descripcion;
		this.existencia = existencia;
		this.nombre = nombre;
		this.precioUnitario = precioUnitario;
		this.tieneImpuesto = tieneImpuesto;
		this.cantCarrito = cantCarrito;
	}
	public Integer getCodigoProducto() {
		return codigoProducto;
	}
	public void setCodigoProducto(Integer codigoProducto) {
		this.codigoProducto = codigoProducto;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public Integer getExistencia() {
		return existencia;
	}
	public void setExistencia(Integer existencia) {
		this.existencia = existencia;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public BigDecimal getPrecioUnitario() {
		return precioUnitario;
	}
	public void setPrecioUnitario(BigDecimal precioUnitario) {
		this.precioUnitario = precioUnitario;
	}
	public String getTieneImpuesto() {
		return tieneImpuesto;
	}
	public void setTieneImpuesto(String tieneImpuesto) {
		this.tieneImpuesto = tieneImpuesto;
	}
	public int getCantCarrito() {
		return cantCarrito;
	}
	public void setCantCarrito(int cantCarrito) {
		this.cantCarrito = cantCarrito;
	}
	
}
