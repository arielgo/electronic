package electronic.model.facturacion.dto;

import java.sql.Date;

public class eventoDTO {

	private Integer idEvento;
	private String descripcion;
	private Date fechaEvento;
	private int idUsuario;
	private String metodo;

	public eventoDTO() {
	}

	public eventoDTO(Integer idEvento, String descripcion, Date fechaEvento, int idUsuario, String metodo) {
		super();
		this.idEvento = idEvento;
		this.descripcion = descripcion;
		this.fechaEvento = fechaEvento;
		this.idUsuario = idUsuario;
		this.metodo = metodo;
	}

	public eventoDTO(Integer idEvento2, String descripcion2, String localeString, int idUsuario2, String metodo2) {
		// TODO Auto-generated constructor stub
	}

	public Integer getIdEvento() {
		return idEvento;
	}

	public void setIdEvento(Integer idEvento) {
		this.idEvento = idEvento;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Date getFechaEvento() {
		return fechaEvento;
	}

	public void setFechaEvento(Date fechaEvento) {
		this.fechaEvento = fechaEvento;
	}

	public int getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getMetodo() {
		return metodo;
	}

	public void setMetodo(String metodo) {
		this.metodo = metodo;
	}

}