package electronic.model.facturacion.dto;

import java.math.BigDecimal;

public class resumenPedidosDTO {

	private int anio;
	private int mes;
	private String estado;
	private BigDecimal Total;

	public resumenPedidosDTO() {

	}

	public resumenPedidosDTO(int anio, int mes, String estado, BigDecimal total) {
		super();
		this.anio = anio;
		this.mes = mes;
		this.estado = estado;
		Total = total;
	}

	public int getAnio() {
		return anio;
	}

	public void setAnio(int anio) {
		this.anio = anio;
	}

	public int getMes() {
		return mes;
	}

	public void setMes(int mes) {
		this.mes = mes;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public BigDecimal getTotal() {
		return Total;
	}

	public void setTotal(BigDecimal total) {
		Total = total;
	}

}
