package electronic.api.rest.evento;

import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import electronic.model.facturacion.dto.eventoDTO;

import minimarketdemo.model.auditoria.managers.ManagerAuditoria;

@RequestScoped
@Path("evento")
@Produces("application/json")
@Consumes("application/json")
public class ServiceRESTEvento {
	@EJB
	private ManagerAuditoria mAuditoria;

	@GET
	@Path(value = "eventos")
	public List<eventoDTO> findAllEventoDTOs() {
		return mAuditoria.findAllEventosDTO();
	}

}