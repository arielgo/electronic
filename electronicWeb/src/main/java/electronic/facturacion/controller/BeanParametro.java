package electronic.facturacion.controller;

import java.util.List;

import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;

import minimarketdemo.controller.JSFUtil;
import minimarketdemo.model.core.entities.Parametro;
import electronic.model.facturacion.manager.ManagerFacturacion;
import java.io.Serializable;

/**
 * ManagedBean JSF para el manejo de parametros.
 * @author mrea
 *
 */
@Named
@SessionScoped
public class BeanParametro implements Serializable {
	private static final long serialVersionUID = 1L;
	@EJB
	private ManagerFacturacion mFacturacion;
	private Parametro edicionParametro;
	
	
	public Parametro getEdicionParametro() {
		return edicionParametro;
	}
	public void setEdicionParametro(Parametro edicionParametro) {
		this.edicionParametro = edicionParametro;
	}
	
	public List<Parametro> getListaParametros(){
		return mFacturacion.findAllParametros();
	}
	public void actionListenerCargarParametro(Parametro p) {
		edicionParametro=p;
	}
	public void actionListenerGuardarEdicionaParametro() {
		try {
			mFacturacion.actualizarParametro(edicionParametro);
			JSFUtil.crearMensajeINFO("Parámetro editado");
		}catch(Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
			
		}
	}
}
