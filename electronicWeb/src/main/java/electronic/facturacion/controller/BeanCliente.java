package electronic.facturacion.controller;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import electronic.model.facturacion.manager.ManagerCliente;
import electronic.model.facturacion.manager.ManagerFacturacion;
import minimarketdemo.controller.JSFUtil;
import minimarketdemo.model.core.entities.Cliente;

@Named
@SessionScoped
public class BeanCliente implements Serializable {
	private static final long serialVersionUID = 1L;
	private List<Cliente> listaCliente;
	private String cedula;
	private String nombres;
	private String apellidos;
	private String direccion;

	@EJB
	private ManagerFacturacion managerFacturacion;
	private ManagerCliente managerCliente;
	

	private Cliente cliente;
	private boolean panelColapso;

	@PostConstruct
	public void inicializar() {
		listaCliente = managerFacturacion.findAllClientes();
		cliente = new Cliente();
		panelColapso = true;
	}

	public void actionListenerColapsarPanel() {
		panelColapso = !panelColapso;
	}

	public String actionActualizarCliente() {
		Cliente c = new Cliente();
		c.setCedulaCliente(cedula);
		c.setNombres(nombres);
		c.setApellidos(apellidos);
		c.setDireccion(direccion);
		try {
			managerFacturacion.actualizarCliente(c);

			cedula = null;
			nombres = "";
			apellidos = "";
			direccion = "";
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
		return "clientes";
	}

	public String actionCargarCliente(Cliente cliente) {
		cedula = cliente.getCedulaCliente();
		nombres = cliente.getNombres();
		apellidos = cliente.getApellidos();
		direccion = cliente.getDireccion();
		return "clientes_update";
	}

	/**
	 * Elimina un cliente de la base de datos
	 * 
	 * @param cliCedula
	 */
	public String actionEliminarCliente(Cliente cliente) {
		try {
			managerFacturacion.eliminarCliente(cliente.getCedulaCliente());
			JSFUtil.crearMensajeINFO("Cliente Eliminado");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
		return "";
	}

	/**
	 * 
	 */
	public String actionInsertarCliente() {
		Cliente c = new Cliente();
		c.setCedulaCliente(cedula);
		c.setNombres(nombres);
		c.setApellidos(apellidos);
		c.setDireccion(direccion);
		try {
			managerFacturacion.insertarCliente(c);
			cedula = null;
			nombres = "";
			apellidos = "";
			direccion = "";
			JSFUtil.crearMensajeINFO("Cliente Agregado");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
		return "";

	}
	public List<Cliente> getListaCliente() {
		listaCliente = managerFacturacion.findAllClientes();
		return listaCliente;
	}

	/**
	 * try{ managerCliente.insertarCliente(cedula,nombres,apellidos,direccion);
	 * JSFUtil.crearMensajeINFO("CLIENTE AGREGADO"); } catch(Exception e) {
	 * JSFUtil.crearMensajeERROR(e.getMessage()); e.printStackTrace(); }
	 * 
	 * }
	 */

	public String getCedula() {
		return cedula;
	}

	public void setCedula(String cedula) {
		this.cedula = cedula;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	

	public void setListaCliente(List<Cliente> listaCliente) {
		this.listaCliente = listaCliente;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public boolean isPanelColapso() {
		return panelColapso;
	}

	public void setPanelColapso(boolean panelColapso) {
		this.panelColapso = panelColapso;
	}

}
