package electronic.facturacion.controller;

import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import minimarketdemo.controller.JSFUtil;
import minimarketdemo.controller.seguridades.BeanSegLogin;
import minimarketdemo.model.core.entities.FacturaCab;
import minimarketdemo.model.core.entities.PedidoCab;
import electronic.model.facturacion.manager.ManagerPedidos;

import java.io.Serializable;

@Named
@SessionScoped
public class BeanSupervisor implements Serializable {
	private static final long serialVersionUID = 1L;
	private Date fechaInicio;
	private Date fechaFinal;
	@EJB
	private ManagerPedidos managerPedidos;
	private PedidoCab pedidoCabTmp;
	private FacturaCab facturaCabTmp;
	private List<PedidoCab> listaPedidoCab;
	
	//Inyeccion de beans manejados:
	@Inject
	private BeanSegLogin beanLogin;
	
	@PostConstruct
	public void inicializar() {
		listaPedidoCab=managerPedidos.findAllPedidoCab();
	}
	
	public BeanSupervisor(){
		
	}
	public String actionBuscar(){
		try {
			//capturamos el valor enviado desde el DataTable:
			listaPedidoCab=managerPedidos.findPedidoCabByFechas(fechaInicio,fechaFinal);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return "";
	}
	/**
	 * 
	 * @param pedidoCab
	 * @return
	 */
	public String actionCargarPedido(PedidoCab pedidoCab){
		try {
			//capturamos el valor enviado desde el DataTable:
			pedidoCabTmp=pedidoCab;
			listaPedidoCab=managerPedidos.findAllPedidoCab();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}
	
	public String actionCancelarPedido(PedidoCab pedidoCab){
		try {
			//invocamos a ManagerFacturacion para crear una nueva factura:
			managerPedidos.anularPedido(beanLogin.getIdSegUsuario(),pedidoCab.getNumeroPedido());
			//managerDAO.actualizar(pedidoCab.getEstadoPedido());
			listaPedidoCab=managerPedidos.findAllPedidoCab();
			JSFUtil.crearMensajeINFO("Pedido anulado correctamente");
		} catch (Exception e) {
			e.printStackTrace();
			JSFUtil.crearMensajeERROR(e.getMessage());
		}
		return "";
	}
	
	public String actionCargarFactura(FacturaCab facturaCab){
		try {
			//capturamos el valor enviado desde el DataTable:
			setFacturaCabTmp(facturaCab);
			listaPedidoCab=managerPedidos.findAllPedidoCab();
		} catch (Exception e) {
			e.printStackTrace();
			JSFUtil.crearMensajeERROR(e.getMessage());
		}
		return "factura_imprimir";
	}
	
	public String actionDespacharPedido(PedidoCab pedidoCab){
		try {
			//invocamos a ManagerFacturacion para crear una nueva factura:
			managerPedidos.despacharPedido(beanLogin.getIdSegUsuario(),pedidoCab.getNumeroPedido());
			JSFUtil.crearMensajeINFO("Pedido despachado correctamente.");
			listaPedidoCab=managerPedidos.findAllPedidoCab();
		} catch (Exception e) {
			e.printStackTrace();
			JSFUtil.crearMensajeERROR(e.getMessage());
		}
		return "";
	}

	public Date getFechaInicio() {
		return fechaInicio;
	}
	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}
	public Date getFechaFinal() {
		return fechaFinal;
	}
	public void setFechaFinal(Date fechaFinal) {
		this.fechaFinal = fechaFinal;
	}
	public PedidoCab getPedidoCabTmp() {
		return pedidoCabTmp;
	}
	public void setPedidoCabTmp(PedidoCab pedidoCabTmp) {
		this.pedidoCabTmp = pedidoCabTmp;
	}
	public List<PedidoCab> getListaPedidoCab() {
		return listaPedidoCab;
	}
	public void setListaPedidoCab(List<PedidoCab> listaPedidoCab) {
		this.listaPedidoCab = listaPedidoCab;
	}

	//Un bean inyectado debe tener sus metodos accesores:

	public FacturaCab getFacturaCabTmp() {
		return facturaCabTmp;
	}

	public BeanSegLogin getBeanLogin() {
		return beanLogin;
	}

	public void setBeanLogin(BeanSegLogin beanLogin) {
		this.beanLogin = beanLogin;
	}

	public void setFacturaCabTmp(FacturaCab facturaCabTmp) {
		this.facturaCabTmp = facturaCabTmp;
	}
	
	
}
