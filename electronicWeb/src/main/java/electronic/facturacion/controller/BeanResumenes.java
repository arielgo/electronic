package electronic.facturacion.controller;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import minimarketdemo.controller.JSFUtil;
import minimarketdemo.model.core.entities.Cliente;
import minimarketdemo.model.core.entities.FacturaCab;
import minimarketdemo.model.core.entities.PedidoCab;
import minimarketdemo.model.core.entities.PedidoDet;
import minimarketdemo.model.core.entities.Producto;
import electronic.model.facturacion.dto.ProductoDTO;
import electronic.model.facturacion.dto.resumenFacturasDTO;
import electronic.model.facturacion.dto.resumenPedidosDTO;
import electronic.model.facturacion.manager.ManagerFacturacion;
import electronic.model.facturacion.manager.ManagerPedidos;
import java.io.Serializable;

@Named
@SessionScoped
public class BeanResumenes implements Serializable {
	private static final long serialVersionUID = 1L;

	private List<resumenFacturasDTO> listaresumenfacturas;
	private List<resumenPedidosDTO> listaresumenPedidos;
	@EJB
	private ManagerFacturacion managerFacturacion;

	public BeanResumenes() {

	}

	@PostConstruct
	public void iniciar() {
		listaresumenfacturas = managerFacturacion.findAllResumenFacturasDTO();
		listaresumenPedidos = managerFacturacion.findAllresumenPedidosDTO();

	}

	public List<resumenPedidosDTO> getListaresumenPedidos() {
		return listaresumenPedidos;
	}

	public void setListaresumenPedidos(List<resumenPedidosDTO> listaresumenPedidos) {
		this.listaresumenPedidos = listaresumenPedidos;
	}

	public List<resumenFacturasDTO> getListaresumenfacturas() {
		return listaresumenfacturas;
	}

	public void setListaresumenfacturas(List<resumenFacturasDTO> listaresumenfacturas) {
		this.listaresumenfacturas = listaresumenfacturas;
	}

}
