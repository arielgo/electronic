package electronic.facturacion.controller;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.inject.Named;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;

import minimarketdemo.controller.JSFUtil;
import minimarketdemo.model.core.entities.Cliente;
import minimarketdemo.model.core.entities.FacturaCab;
import minimarketdemo.model.core.entities.Producto;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import electronic.model.facturacion.manager.ManagerFacturacion;

/**
 * ManagedBean JSF para el manejo de la facturacion.
 * 
 * @author mrea
 *
 */

@Named
@SessionScoped
public class BeanFactura implements Serializable {
	private static final long serialVersionUID = 1L;
	private String cedulaCliente;
	private int codigoVendedor;
	@EJB
	private ManagerFacturacion managerFacturacion;
	private Integer codigoProducto;
	private Integer cantidadProducto;
	private FacturaCab facturaCabTmp;
	private boolean facturaCabTmpGuardada;

	private int cantExistenciaProducto;

	public BeanFactura() {

	}

	@PostConstruct
	public void inicializar() {
		facturaCabTmp = managerFacturacion.crearFacturaTmp();
		cantExistenciaProducto = 0;
	}

	/**
	 * Action para la creacion de una factura temporal en memoria. Hace uso del
	 * componente {@link facturacion.model.manager.ManagerFacturacion
	 * ManagerFacturacion} de la capa model.
	 * 
	 * @return outcome para la navegacion.
	 */
	public String crearNuevaFactura() {
		facturaCabTmp = managerFacturacion.crearFacturaTmp();
		codigoVendedor = 0;
		cedulaCliente = null;
		codigoProducto = 0;
		cantidadProducto = 0;
		facturaCabTmpGuardada = false;
		return "";
	}

	/**
	 * Action para asignar un vendedor a la factura temporal actual. Hace uso del
	 * componente {@link facturacion.model.manager.ManagerFacturacion
	 * ManagerFacturacion} de la capa model.
	 * 
	 * @return outcome para la navegacion.
	 */
	public void asignarVendedor() {
		if (facturaCabTmpGuardada == true) {
			JSFUtil.crearMensajeWARN("La factura ya fue guardada.");
		}
		try {
			managerFacturacion.asignarVendedorFacturaTmp(facturaCabTmp, codigoVendedor);
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
		}
	}

	/**
	 * Action para asignar un cliente a la factura temporal actual. Hace uso del
	 * componente {@link facturacion.model.manager.ManagerFacturacion
	 * ManagerFacturacion} de la capa model.
	 * 
	 * @return outcome para la navegacion.
	 */
	public void asignarCliente() {
		if (facturaCabTmpGuardada == true) {
			JSFUtil.crearMensajeWARN("La factura ya fue guardada.");
		}
		try {
			managerFacturacion.asignarClienteFacturaTmp(facturaCabTmp, cedulaCliente);
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
		}
	}

	public void verificarExistencia() {
		try {
			if (managerFacturacion.obtenerExistencia(codigoProducto) == 0)
				JSFUtil.crearMensajeERROR("No hay existencia");
			else
				cantExistenciaProducto = (int) managerFacturacion.findProductoById(codigoProducto).getExistencia();
			System.out.println("cantidad es la siguiente :" + cantExistenciaProducto);
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}

	/**
	 * Action que adiciona un item a una factura temporal. Hace uso del componente
	 * {@link model.manager.ManagerFacturacion ManagerFacturacion} de la capa model.
	 * 
	 * @return
	 */
	public void insertarDetalle() {
		if (facturaCabTmpGuardada == true) {
			JSFUtil.crearMensajeWARN("La factura ya fue guardada.");

		}
		try {
			managerFacturacion.agregarDetalleFacturaTmp(facturaCabTmp, codigoProducto, cantidadProducto);
			codigoProducto = 0;
			cantidadProducto = 0;
			JSFUtil.crearMensajeINFO("Producto añadido");

		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
		}

	}
	public String actionReporte(){
		 Map<String,Object> parametros=new HashMap<String,Object>();
		 /*parametros.put("p_titulo_principal",p_titulo_principal);
		 parametros.put("p_titulo",p_titulo);*/
		 FacesContext context=FacesContext.getCurrentInstance();
		 ServletContext servletContext=(ServletContext)context.getExternalContext().getContext();
		 String ruta=servletContext.getRealPath("gerente/reporte.jasper");
		 System.out.println(ruta);
		 HttpServletResponse response=(HttpServletResponse)context.getExternalContext().getResponse();
		 response.addHeader("Content-disposition", "attachment;filename=reporte.pdf");
		 response.setContentType("application/pdf");
		 try {
		 Class.forName("org.postgresql.Driver");
		 Connection connection = null;
		 connection = DriverManager.getConnection(
		 "jdbc:postgresql://192.100.198.141:5432/narvaez","mipymes", "prueba21.");
		 JasperPrint impresion=JasperFillManager.fillReport(ruta, parametros,connection);
		 JasperExportManager.exportReportToPdfStream(impresion, response.getOutputStream());
		 context.getApplication().getStateManager().saveView ( context ) ;
		 System.out.println("reporte generado.");
		 context.responseComplete();
		 } catch (Exception e) {
		 JSFUtil.crearMensajeERROR(e.getMessage());
		 e.printStackTrace();
		 }
		 return "";
		 }


	public String eliminarDetalle(int numeroFacturaDet) {
		try {

			managerFacturacion.eliminarDetalleFacturaTemp(facturaCabTmp, numeroFacturaDet);
			JSFUtil.crearMensajeWARN("Producto Eliminado .");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			JSFUtil.crearMensajeERROR(e.getMessage());
		}

		return "";
	}

	/**
	 * Action que almacena en la base de datos una factura temporal creada en
	 * memoria. Hace uso del componente
	 * {@link facturacion.model.manager.ManagerFacturacion ManagerFacturacion} de la
	 * capa model.
	 * 
	 * @return outcome para la navegacion.
	 */
	
	public void guardarFactura(int codigoVendedor) {
		setCodigoVendedor(codigoVendedor);
		asignarVendedor();
		if (facturaCabTmpGuardada == true) {
			JSFUtil.crearMensajeWARN("La factura ya fue guardada.");

		}
		try {
			managerFacturacion.guardarFacturaTemporal(facturaCabTmp);
			facturaCabTmpGuardada = true;
			JSFUtil.crearMensajeINFO("Factura guardada.");

		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
		}

	}

	public String getCedulaCliente() {
		return cedulaCliente;
	}

	public void setCedulaCliente(String cedulaCliente) {
		this.cedulaCliente = cedulaCliente;
	}

	public Integer getCodigoProducto() {
		return codigoProducto;
	}

	public void setCodigoProducto(Integer codigoProducto) {
		this.codigoProducto = codigoProducto;
	}

	public Integer getCantidadProducto() {
		return cantidadProducto;
	}

	public void setCantidadProducto(Integer cantidadProducto) {
		this.cantidadProducto = cantidadProducto;
	}

	/**
	 * Devuelve un listado de componentes SelectItem a partir de un listado de
	 * {@link facturacion.model.dao.entities.Cliente Cliente}.
	 * 
	 * @return listado de SelectItems de clientes.
	 */
	public List<SelectItem> getListaClientesSI() {
		List<SelectItem> listadoSI = new ArrayList<SelectItem>();
		List<Cliente> listadoClientes = managerFacturacion.findAllClientes();

		for (Cliente c : listadoClientes) {
			SelectItem item = new SelectItem(c.getCedulaCliente(), c.getApellidos() + " " + c.getNombres());
			listadoSI.add(item);
		}
		return listadoSI;
	}

	/**
	 * Devuelve un listado de componentes SelectItem a partir de un listado de
	 * {@link facturacion.model.dao.entities.Producto Producto}.
	 * 
	 * @return listado de SelectItems de productos.
	 */
	public List<SelectItem> getListaProductosSI() {
		List<SelectItem> listadoSI = new ArrayList<SelectItem>();
		List<Producto> listadoProductos = managerFacturacion.findAllProductos();

		for (Producto p : listadoProductos) {
			SelectItem item = new SelectItem(p.getCodigoProducto(), p.getNombre());
			listadoSI.add(item);
		}
		return listadoSI;
	}

	public FacturaCab getFacturaCabTmp() {
		return facturaCabTmp;
	}

	public void setFacturaCabTmp(FacturaCab facturaCabTmp) {
		this.facturaCabTmp = facturaCabTmp;
	}

	public List<FacturaCab> getListaFacturasCab() {
		List<FacturaCab> listadoFacturas = managerFacturacion.findAllFacturaCab();
		return listadoFacturas;
	}

	public List<FacturaCab> getListaFacturasVendedor(int codigo) {
		List<FacturaCab> listadoFacturasVen = managerFacturacion.findListasVendedor(codigo);
		return listadoFacturasVen;
	}

	public List<FacturaCab> getListaFacturasCliente(String cedula) {
		List<FacturaCab> listadoFacturas = managerFacturacion.findAllFacturaByCI(cedula);
		return listadoFacturas;
	}

	public boolean isFacturaCabTmpGuardada() {
		return facturaCabTmpGuardada;
	}

	public void setFacturaCabTmpGuardada(boolean facturaCabTmpGuardada) {
		this.facturaCabTmpGuardada = facturaCabTmpGuardada;
	}

	public int getCodigoVendedor() {
		return codigoVendedor;
	}

	public void setCodigoVendedor(int codigoVendedor) {
		this.codigoVendedor = codigoVendedor;
	}

	public int getCantExistenciaProducto() {
		return cantExistenciaProducto;
	}

	public void setCantExistenciaProducto(int cantExistenciaProducto) {
		this.cantExistenciaProducto = cantExistenciaProducto;
	}

}
