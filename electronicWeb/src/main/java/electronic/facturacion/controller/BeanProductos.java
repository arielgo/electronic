package electronic.facturacion.controller;

import java.math.BigDecimal;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import electronic.model.facturacion.manager.ManagerFacturacion;
import minimarketdemo.controller.JSFUtil;
import minimarketdemo.model.core.entities.Producto;
import java.io.Serializable;

@Named
@SessionScoped
public class BeanProductos implements Serializable {
	private static final long serialVersionUID = 1L;
	private List<Producto> listaProductos;
	@EJB
	private ManagerFacturacion managerFacturacion;

	private Integer codigoProducto;
	private String descripcion;
	private Integer existencia;
	private String nombre;
	private BigDecimal precioUnitario;
	private String tieneImpuesto;
	private Integer codigo; 

	public BeanProductos()  {
			
		
	}
	
	@PostConstruct
	public void iniciar(){
		try {
			
			this.codigo = generarCodigo();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public String actionInsertarProducto() throws Exception {
		// try {
		// if( precioUnitario.equals(precioUnitario.ZERO))
		// JSFUtil.crearMensajeERROR("el precio es cero ");
		// lo comente porque marcaba error
		//generarCodigo();
		Producto p = new Producto();
		p.setCodigoProducto(generarCodigo());
		p.setDescripcion(descripcion);
		p.setExistencia(existencia);
		p.setNombre(nombre);
		p.setPrecioUnitario(precioUnitario);
		p.setTieneImpuesto(tieneImpuesto);
		try {
			managerFacturacion.insertarProducto(p);
			// limpiamos las variables del formulario:
			codigoProducto = null;
			descripcion = "";
			existencia = null;
			nombre = "";
			precioUnitario = null;
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
		return "";
	}

	public String actionEliminarProducto(Producto producto) {
		try {
			managerFacturacion.eliminarProducto(producto.getCodigoProducto());
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
		return "";
	}

	public String actionCargarProducto(Producto producto) {
		codigoProducto = producto.getCodigoProducto();
		descripcion = producto.getDescripcion();
		nombre = producto.getNombre();
		precioUnitario = producto.getPrecioUnitario();
		existencia = producto.getExistencia();
		tieneImpuesto = producto.getTieneImpuesto();
		return "productos_update";
	}

	public String actionActualizarProducto() {
		Producto p = new Producto();
		p.setCodigoProducto(codigoProducto);
		p.setNombre(nombre);
		p.setDescripcion(descripcion);
		p.setPrecioUnitario(precioUnitario);
		p.setExistencia(existencia);
		p.setTieneImpuesto(tieneImpuesto);
		try {
			managerFacturacion.actualizarProducto(p);
			// limpiamos las variables del formulario:
			codigoProducto = null;
			descripcion = "";
			existencia = null;
			nombre = "";
			precioUnitario = null;
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
		return "productos";
	}

	public List<Producto> getListaProductos() {
		listaProductos = managerFacturacion.findAllProductos();
		return listaProductos;
	}

	public Integer getCodigoProducto() {
		return codigoProducto;
	}

	public void setCodigoProducto(Integer codigoProducto) {
		this.codigoProducto = codigoProducto;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Integer getExistencia() {
		return existencia;
	}

	public void setExistencia(Integer existencia) {
		this.existencia = existencia;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public BigDecimal getPrecioUnitario() {
		return precioUnitario;
	}

	public void setPrecioUnitario(BigDecimal precioUnitario) {
		this.precioUnitario = precioUnitario;
	}

	public String getTieneImpuesto() {
		return tieneImpuesto;
	}

	public void setTieneImpuesto(String tieneImpuesto) {
		this.tieneImpuesto = tieneImpuesto;
	}
	

	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	int generarCodigo() throws Exception  {
		
		int serie = managerFacturacion.generarCodigo()+1;
	
		return serie;
		
		
	}

}
