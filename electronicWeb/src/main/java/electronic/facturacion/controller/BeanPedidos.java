package electronic.facturacion.controller;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import minimarketdemo.controller.JSFUtil;
import minimarketdemo.model.core.entities.Cliente;
import minimarketdemo.model.core.entities.FacturaCab;
import minimarketdemo.model.core.entities.PedidoCab;
import minimarketdemo.model.core.entities.PedidoDet;
import minimarketdemo.model.core.entities.Producto;
import electronic.model.facturacion.dto.ProductoDTO;
import electronic.model.facturacion.manager.ManagerFacturacion;
import electronic.model.facturacion.manager.ManagerPedidos;
import java.io.Serializable;


@Named
@SessionScoped
public class BeanPedidos implements Serializable {
	private static final long serialVersionUID = 1L;
	private String cedula;
	private String nombres;
	private String apellidos;
	private String direccion;
	private String clave;
	private int cantCarrito;
	
	private List<Producto> listaProductos;
	private List<ProductoDTO> listaProductosDTO;
	@EJB
	private ManagerFacturacion managerFacturacion;
	@EJB
	private ManagerPedidos managerPedidos;
	
	private PedidoCab pedidoCabTmp;

	public BeanPedidos() {

	}
	@PostConstruct
	public void iniciar(){
		
		listaProductos=managerFacturacion.findAllProductos();
		listaProductosDTO=managerFacturacion.findAllProductosDTO();
	}

	public String actionComprobarCedula() {
		try {
			Cliente c = managerFacturacion.findClienteById(cedula);
			// verificamos la existencia del cliente:
			if (c == null)
				return "registro";// debe registrarse
			
			//caso contrario si ya existe el cliente, recuperamos la informacion
			// para presentarla en la pagina de pedidos:
			nombres = c.getNombres();
			apellidos = c.getApellidos();
			direccion = c.getDireccion();
			//creamos el pedido temporal y asignamos el cliente automaticamente:
			pedidoCabTmp=managerPedidos.crearPedidoTmp();
			managerPedidos.asignarClientePedidoTmp(pedidoCabTmp, cedula);

			return "pedido";
		} catch (Exception e) {
			// error no esperado:
			e.printStackTrace();
			JSFUtil.crearMensajeERROR(e.getMessage());
			return "";
		}
	}

	public String actionInsertarCliente() {
		try {
			managerFacturacion.insertarCliente(cedula, nombres, apellidos,
					direccion);
			JSFUtil.crearMensajeINFO("Cliente agregado.");
			return "pedido";
			
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
		return "";

	}
	
	public void actionInsertarProducto(ProductoDTO p){
		try {
			if(pedidoCabTmp==null)
				pedidoCabTmp=managerPedidos.crearPedidoTmp();
			//agregamos un nuevo producto al carrito de compras:
			managerPedidos.agregarDetallePedidoTmp(pedidoCabTmp, p.getCodigoProducto(), p.getCantCarrito());
			System.out.println(p.getCantCarrito());
			JSFUtil.crearMensajeINFO("Producto agregado.");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void actionEliminarProducto(PedidoDet pedidoDet) {
		try {
			///Eliminamos el producto del carrito de compras
			managerPedidos.eliminarDetallePedidoTemp(pedidoDet, pedidoCabTmp);
			JSFUtil.crearMensajeINFO("Producto eliminado exitosamente.");
		} catch (Exception e) {
			e.printStackTrace();
			JSFUtil.crearMensajeERROR(e.getMessage());
		}
	}
	
	public String actionGuardarPedido(){
		try {
			managerPedidos.adicionarcompra(pedidoCabTmp);
		} catch (Exception e) {
			e.printStackTrace();
			JSFUtil.crearMensajeERROR(e.getMessage());
		}
		JSFUtil.crearMensajeINFO("Pedido realizado exitosamente.");
		return "pedido_imprimir";
	}
	
	public List<PedidoCab> getListaPedidosCliente(String cedula){
		List<PedidoCab> listadoPedidos = managerPedidos.findAllPedidosByCI(cedula);
		return listadoPedidos;
		
	}
	
	public String actionCerrarPedido(){
		pedidoCabTmp=null;
		//creamos el pedido temporal y asignamos el cliente automaticamente:
		pedidoCabTmp=managerPedidos.crearPedidoTmp();
		
		try {
			managerPedidos.asignarClientePedidoTmp(pedidoCabTmp, cedula);
		} catch (Exception e) {
			e.printStackTrace();
			JSFUtil.crearMensajeERROR(e.getMessage());
		}
		return "pedido";
	}
	
	public String actionContinuar(){
		try {
			managerPedidos.continuar(pedidoCabTmp);
			return "confirmacion";
		} catch (Exception e) {
			e.printStackTrace();
			JSFUtil.crearMensajeERROR(e.getMessage());
			return "";	
		}
	}
	

	public String getCedula() {
		return cedula;
	}

	public void setCedula(String cedula) {
		this.cedula = cedula;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	public List<Producto> getListaProductos() {
		return listaProductos;
	}

	public void setListaProductos(List<Producto> listaProductos) {
		this.listaProductos = listaProductos;
	}

	public PedidoCab getPedidoCabTmp() {
		return pedidoCabTmp;
	}

	public void setPedidoCabTmp(PedidoCab pedidoCabTmp) {
		this.pedidoCabTmp = pedidoCabTmp;
	}
	public int getCantCarrito() {
		return cantCarrito;
	}
	public void setCantCarrito(int cantCarrito) {
		this.cantCarrito = cantCarrito;
	}
	public List<ProductoDTO> getListaProductosDTO() {
		return listaProductosDTO;
	}
	public void setListaProductosDTO(List<ProductoDTO> listaProductosDTO) {
		this.listaProductosDTO = listaProductosDTO;
	}

}
